window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      const selectTag = document.getElementById("conference");
      for (let conference of data.conferences) {
        const option = document.createElement("option");
        option.innerHTML = conference.name;
        option.value = conference.id;
        selectTag.appendChild(option);
        console.log(conference)
      }
      const formTag = document.getElementById("create-presentation-form");
      formTag.addEventListener("submit", async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const confid = formData.get('conference')
        const presentationUrl = `http://localhost:8000/api/conferences/${confid}/presentations/`;
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            "Content-Type": "application.json",
          },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
          formTag.reset();
          const newPresentation = await response.json();
          console.log(newPresentation);
        }
      });
    }
  });