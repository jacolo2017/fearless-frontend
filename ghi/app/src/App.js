import Nav from './Nav'
import AtttendeesList from './AttendeesList'
import ConferenceForm from './ConferenceForm'
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import { BrowserRouter } from 'react-router-dom';
import { Routes } from 'react-router-dom';
import { Route } from 'react-router-dom';
import PresentationForm from './PresentationForm'
import MainPage from './MainPage'

function App(props) {
  if (props.attendees === undefined) {
    return null
  }
  return (
  <BrowserRouter>
    <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path='locations/new' element={<LocationForm />} />
        <Route path='conference/new' element={<ConferenceForm />} />
        <Route path='attendees' element={<AtttendeesList attendees={props.attendees} />}/>
        <Route path='attendees/new' element={<AttendConferenceForm />}/>
        <Route path='presentation/new' element={<PresentationForm />}></Route>
      </Routes>
  </BrowserRouter>
  );
}

export default App;
