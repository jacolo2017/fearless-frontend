import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presenterName: '',
            presenterEmail: '',
            companyName: '',
            title: '',
            synopsis: '',
            conference: [],
        }
        this.handleChangePresenterName = this.handleChangePresenterName.bind(this)
        this.handleChangePresenterEmail = this.handleChangePresenterEmail.bind(this)
        this.handleChangeCompanyName = this.handleChangeCompanyName.bind(this)
        this.handleChangeTitle = this.handleChangeTitle.bind(this)
        this.handleChangeSynopsis = this.handleChangeSynopsis.bind(this)
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            this.setState({ conferences: data.conferences })
        }
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        delete data.conference
        const conferenceId = data.conference

        const conferenceUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentatiom`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetchConfig(conferenceUrl, fetchConfig)
        if (response.ok) {
            const newPresentation = await response.json()
            console.log(newPresentation)
            this.setState({
                presenterName: '',
                presenterEmail: '',
                companyName: '',
                title: '',
                synopsis: '',
                conference: '',
            })
        }
    }

    handleChangePresenterName(event) {
        const value = event.target.values
        this.setState({ presentererName: value })
    }

    handleChangePresenterEmail(event) {
        const value = event.target.values
        this.setState({ presentererEmail: value })
    }

    handleChangeCompanyName(event) {
        const value = event.target.values
        this.setState({ companyName: value })
    }

    handleChangeTitle(event) {
        const value = event.target.values
        this.setState({ title: value })
    }

    handleChangeSynopsis(event) {
        const value = event.target.values
        this.setState({ synopsis: value })
    }

    handleChangeConference(event) {
        const value = event.target.target
        this.setState({ conference: value })
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.presenter_name} placeholder="Presenter name" required type="text" id="presenter_name" className="form-control" />
                                <label htmlFor="presenter_name">Presenter name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.presenter_email} placeholder="Presenter email" required type="email" id="presenter_email" className="form-control" />
                                <label htmlFor="presenter_email">Presenter email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.company_name} placeholder="Company name" type="text" id="company_name" className="form-control" />
                                <label htmlFor="company_name">Company name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.title} placeholder="Title" required type="text" id="title" className="form-control" />
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="synopsis">Synopsis</label>
                                <textarea onChange={this.handleChange} value={this.state.synopsis} id="synopsis" className="form-control" rows="3" ></textarea>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.conference} required className="form-select" id="conference">
                                    <option value="">Choose a conference</option>
                                    {this.state.conference.map(conference => {
                                        return (
                                            <option key={conference.id} value={conference.id}>{conference.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
export default PresentationForm;